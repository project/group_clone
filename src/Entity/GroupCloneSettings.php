<?php

declare(strict_types=1);

namespace Drupal\group_clone\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the group_clone_settings config entity type.
 *
 * @ConfigEntityType(
 *   id = "group_clone_settings",
 *   label = @Translation("Group clone settings"),
 *   config_prefix = "group_clone",
 *   admin_permission = "administer group",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "status",
 *     "field_config",
 *     "plugin_config",
 *   },
 * )
 */
final class GroupCloneSettings extends ConfigEntityBase {

  /**
   * The group type ID.
   *
   * @var string
   */
  protected ?string $id;

  /**
   * Configuration of group field cloning.
   *
   * @var bool[]
   */
  protected array $field_config = [];

  /**
   * Configuration of content cloner plugins.
   *
   * @var mixed[]
   */
  protected array $plugin_config = [];

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): self {
    parent::calculateDependencies();

    $group_type = $this->entityTypeManager()
      ->getStorage('group_type')
      ->load($this->id);
    $this->addDependency('config', $group_type->getConfigDependencyName());

    foreach ($this->plugin_config as $plugin_id => $plugin_config) {
      $plugin = \Drupal::service('plugin.manager.group_content_cloner')->createInstance($plugin_id, $plugin_config);
      foreach ($plugin->calculateDependencies($group_type) as $type => $dependency) {
        $this->addDependency($type, $dependency);
      }
    }

    return $this;
  }

  /**
   * Check if settings are empty.
   */
  public function empty(): bool {
    if ($this->status) {
      return FALSE;
    }

    return TRUE;
  }

}
