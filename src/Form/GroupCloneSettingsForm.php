<?php

declare(strict_types=1);

namespace Drupal\group_clone\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\group_clone\EntityCloneTrait;
use Drupal\group_clone\GroupContentClonerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
final class GroupCloneSettingsForm extends FormBase {

  use GroupCloneFormTrait;
  use EntityCloneTrait;

  /**
   * Constructs a new GroupCloneSettingsForm object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly RouteBuilderInterface $routeBuilder,
    private readonly GroupContentClonerManager $clonerManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('router.builder'),
      $container->get('plugin.manager.group_content_cloner')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_clone_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();

    $plugins = $this->clonerManager->getPlugins();

    foreach ($group_types as $group_type) {
      $id = $group_type->id();
      $config_entity = $this->getConfigEntity($id);

      $form[$id] = [
        '#tree' => TRUE,
        '#type' => 'details',
        '#open' => $config_entity->get('status'),
        '#title' => $group_type->label(),
        'plugin_settings' => [],
      ];
      $form[$id]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $config_entity->get('status'),
      ];

      // Group field cloning settings.
      $field_config = $config_entity->get('field_config');
      if ($field_config === NULL) {
        $field_config = [];
      }
      $field_options = [];
      foreach ($this->entityFieldManager->getFieldDefinitions('group', $id) as $field_id => $definition) {
        if ($this->fieldIsClonable($definition)) {
          $field_options[$field_id] = $definition->label();
          if (\array_key_exists($field_id, $field_config)) {
            $field_config[$field_id] = $field_config[$field_id] ? $field_id : 0;
          }
          else {
            $field_config[$field_id] = 0;
          }
        }
      }
      if (\count($field_options) !== 0) {
        $form[$id]['field_config'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Group fields config'),
          '#description' => $this->t('Check to clone entities referenced from this field (not recommended in most cases, this will clone referenced images, taxonomy terms etc. If not checked references will be preserved).'),
          '#options' => $field_options,
          '#default_value' => $field_config,
        ];
      }

      $form[$id]['plugin_config'] = [];
      $this->getPluginsConfigForm($form[$id]['plugin_config'], $plugins, $config_entity, $group_type, $form_state);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();
    $plugins = $this->clonerManager->getPlugins();
    foreach ($form_state->getValues() as $id => $settings_array) {
      foreach ($plugins as $plugin_id => $plugin) {
        if (\array_key_exists($plugin_id, $settings_array['plugin_config'])) {
          $plugin->setConfiguration($settings_array['plugin_config'][$plugin_id]);
          $settings_array['plugin_config'][$plugin_id] = $plugin->getConfiguration();
        }
      }

      $config_entity = $this->getConfigEntity($id);
      foreach ($settings_array as $property => $value) {
        $config_entity->set($property, $value);
      }

      if (!$config_entity->empty()) {
        $config_entity->save();
      }
      elseif (!$config_entity->isNew()) {
        $config_entity->delete();
      }
    }

    // Rebuild routes as the one defined by this module depends on those
    // config entities.
    $this->routeBuilder->rebuild();

    $this->messenger()->addStatus($this->t('Group clone setting saved.'));
  }

}
