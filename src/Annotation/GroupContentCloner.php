<?php

declare(strict_types=1);

namespace Drupal\group_clone\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Group content cloner annotation object.
 *
 * Plugin Namespace: Plugin\GroupContentCloner.
 *
 * @Annotation
 */
final class GroupContentCloner extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the action plugin.
   *
   * @var label
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The group content provider base plugin ID this cloner applies to.
   *
   * @var string
   */
  public string $content_plugin_id = '';

}
