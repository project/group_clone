<?php

declare(strict_types=1);

namespace Drupal\group_clone;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritdoc}
 */
final class GroupClonerBatch {

  /**
   * Set message function wrapper.
   *
   * @see \Drupal\Core\Messenger\MessengerInterface
   */
  public static function message(TranslatableMarkup $message, ?string $type = 'status', ?bool $repeat = TRUE): void {
    \Drupal::messenger()->addMessage($message, $type, $repeat);
  }

  /**
   * Translation function wrapper.
   *
   * @see \Drupal\Core\StringTranslation\TranslationInterface:translate()
   */
  public static function translate(string $string, ?array $args = [], ?array $options = []): TranslatableMarkup {
    return \Drupal::translation()->translate($string, $args, $options);
  }

  /**
   * Batch builder.
   *
   * @param mixed[] $data
   *   Data.
   *
   * @return mixed[]
   *   $batch
   */
  public static function getCloneBatch(array $data): array {
    $batch = [
      'title' => static::translate('Cloning content of @group_label group.', ['@group_label' => $data['group_label']]),
      'operations' => [
        [
          [\get_called_class(), 'cloneOperation'],
          [$data],
        ],
      ],
      'progress_message' => static::translate('Processing, estimated time left: @estimate, elapsed: @elapsed.'),
      'finished' => [\get_called_class(), 'cloningFinished'],
    ];

    return $batch;
  }

  /**
   * Batch clone operation callback.
   *
   * @param mixed[] $data
   *   Processed view data.
   * @param mixed[] $context
   *   Batch context.
   */
  public static function cloneOperation(array $data, array &$context): void {
    // Initialize batch.
    // Group entity itself has already been cloned at this point.
    if (empty($context['sandbox'])) {
      $context['sandbox']['processed'] = 0;
      $context['sandbox']['content_items'] = $data['content_items'];
      $context['sandbox']['total'] = count($data['content_items']);
      $context['results']['original_group_id'] = $data['original_group_id'];
      $context['results']['cloned_entity_ids'] = $data['cloned_entity_ids'];
      $context['results']['group_clone_id'] = $data['group_clone_id'];
      $context['results']['group_label'] = $data['group_label'];
    }

    if (count($context['sandbox']['content_items']) === 0) {
      return;
    }

    // Initialize cloner.
    $cloner = \Drupal::service('group_clone.cloner');
    $cloner->setCloningSettings($data['settings']);
    $cloner->setClonedEntitityIds($context['results']['cloned_entity_ids']);

    // Clone group content entity.
    $cloned_item = array_shift($context['sandbox']['content_items']);
    $cloner->cloneItem($cloned_item, $data['group_clone_id']);
    $context['results']['cloned_entity_ids'] = $cloner->getClonedEntitityIds();

    // Set batch variables.
    $context['sandbox']['processed']++;
    $context['finished'] = $context['sandbox']['processed'] / $context['sandbox']['total'];
    $context['message'] = static::translate('Cloned @count of @total group content items.', [
      '@count' => $context['sandbox']['processed'],
      '@total' => $context['sandbox']['total'],
    ]);
  }

  /**
   * Cloning finished callback.
   *
   * @param bool $success
   *   Was the process successful?
   * @param array $results
   *   Batch process results array.
   * @param array $operations
   *   Performed operations array.
   */
  public static function cloningFinished(bool $success, array $results, array $operations): RedirectResponse {
    $cloner = \Drupal::service('group_clone.cloner');

    // Store cloned data to allow reverting regardless of the success / failure
    // status.
    $cloner->storeClonedData($results['original_group_id'], $results['group_clone_id'], $results['cloned_entity_ids']);

    if ($success) {
      $message = $cloner->getResultsMessage($results);
      static::message($message);
      $cloned_group = \Drupal::entityTypeManager()->getStorage('group')->load($results['group_clone_id']);
      return new RedirectResponse($cloned_group->toUrl('edit-form')->toString());
    }
    else {
      $message = $cloner->getResultsMessage([], new \Exception('batch process error'));
      static::message($message, 'error');
      return new RedirectResponse(Url::fromRoute('entity.group.duplicate_form', [
        'group' => $results['group_clone_id'],
      ])->toString());
    }
  }

  /**
   * Revert batch builder.
   *
   * @param mixed[] $data
   *   Data.
   *
   * @return mixed[]
   *   $batch
   */
  public static function getRevertBatch(array $data): array {
    $batch = [
      'title' => static::translate('Deleting @group_label group and all cloned content.', ['@group_label' => $data['group_label']]),
      'operations' => [
        [
          [\get_called_class(), 'revertOperation'],
          [$data],
        ],
      ],
      'progress_message' => static::translate('Deleting, estimated time left: @estimate, elapsed: @elapsed.'),
      'finished' => [\get_called_class(), 'revertFinished'],
    ];

    return $batch;
  }

  /**
   * Batch revert operation callback.
   *
   * @param mixed[] $data
   *   Processed view data.
   * @param mixed[] $context
   *   Batch context.
   */
  public static function revertOperation(array $data, array &$context): void {
    if (empty($context['sandbox'])) {
      $context['sandbox']['processed'] = 0;
      $context['sandbox']['processing_list'] = [];
      $context['results']['redirect_url'] = $data['redirect_url'];
      $context['results']['group_label'] = $data['group_label'];
      $context['results']['group_clone_id'] = $data['group_clone_id'];
      foreach ($data['cloned_entity_ids'] as $entity_type_id => $type_entity_ids) {
        foreach ($type_entity_ids as $entity_id) {
          $context['sandbox']['processing_list'][] = [
            'entity_type_id' => $entity_type_id,
            'entity_id' => $entity_id,
          ];
        }
      }
      $context['sandbox']['total'] = count($context['sandbox']['processing_list']);
    }

    $item = array_pop($context['sandbox']['processing_list']);
    if (is_array($item)) {
      $cloner = \Drupal::service('group_clone.cloner');
      $cloner->revertSingle($item['entity_type_id'], $item['entity_id']);

      $context['sandbox']['processed']++;
      $context['finished'] = $context['sandbox']['processed'] / $context['sandbox']['total'];
      $context['message'] = static::translate('Deleted @count of @total cloned entities.', [
        '@count' => $context['sandbox']['processed'],
        '@total' => $context['sandbox']['total'],
      ]);
    }
  }

  /**
   * Revert finished callback.
   *
   * @param bool $success
   *   Was the process successful?
   * @param array $results
   *   Batch process results array.
   * @param array $operations
   *   Performed operations array.
   */
  public static function revertFinished(bool $success, array $results, array $operations): ?RedirectResponse {
    if ($success) {
      \Drupal::state()->delete(GroupCloner::STATE_PREFIX . $results['group_clone_id']);
      static::message(static::translate('Removed @group group and its content.', [
        '@group' => $results['group_label'],
      ]));
    }
    else {
      static::message(static::translate('Finished with an error.'), 'error');
    }
    return new RedirectResponse($results['redirect_url']);
  }

}
