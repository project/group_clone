<?php

declare(strict_types=1);

namespace Drupal\group_clone;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * {@inheritdoc}
 */
final class GroupContentClonerManager extends DefaultPluginManager {

  /**
   * Constructs a new class instance.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct('Plugin/GroupContentCloner', $namespaces, $module_handler, 'Drupal\group_clone\Plugin\GroupContentCloner\GroupContentClonerInterface', 'Drupal\group_clone\Annotation\GroupContentCloner');
    $this->alterInfo('group_content_cloner');
    $this->setCacheBackend($cache_backend, 'group_content_cloner');
  }

  /**
   * Gets the plugin definitions for this group content base plugin ID.
   *
   * @return mixed[]
   *   $definition[]
   */
  public function getDefinitionsByPlugin(string $plugin_id): array {
    return \array_filter($this->getDefinitions(), function ($definition) use ($plugin_id) {
      return $definition['content_plugin_id'] === $plugin_id;
    });
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    if (\array_keys($definitions)[0] === 'default') {
      return $definitions;
    }

    // Order so the default plugin is first and can be overridden.
    $default = $definitions['default'];
    unset($definitions['default']);
    $sorted = ['default' => $default];
    foreach ($definitions as $id => $definition) {
      $sorted[$id] = $definition;
    }

    return $sorted;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugins(): array {
    $plugins = [];
    foreach (\array_keys($this->getDefinitions()) as $plugin_id) {
      $plugins[$plugin_id] = $this->createInstance($plugin_id, []);
    }
    return $plugins;
  }

}
