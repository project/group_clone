<?php

declare(strict_types=1);

namespace Drupal\group_clone;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_clone\Plugin\GroupContentCloner\GroupContentClonerInterface;

/**
 * {@inheritdoc}
 */
final class GroupCloner {

  use EntityCloneTrait;
  use StringTranslationTrait;

  public const STATE_PREFIX = 'group_clone_data.';
  public const PRESERVE_VALUE = '#preserve#';

  /**
   * {@inheritdoc}
   */
  private GroupContentClonerManager $clonerManager;

  /**
   * {@inheritdoc}
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  private AccountProxyInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  private StateInterface $state;

  /**
   * Cloning settings keyed by group content type.
   *
   * @var int[]
   */
  private array $settings = [];

  /**
   * Group content type - base plugin ID map.
   *
   * Data caching purpose.
   *
   * @var string[]
   */
  private array $pluginMap = [];

  /**
   * Cloner plugins.
   *
   * @var \Drupal\group_clone\Plugin\GroupContentCloner\GroupContentClonerInterface[]
   */
  private array $clonerPlugins = [];

  /**
   * Cloned entities array for referencing.
   *
   * Keyed by entity type ID as it can contain all types of entities
   * and not only group_content ones.
   *
   * @var mixed[]
   */
  private array $clonedEntityIDs = [];

  /**
   * Is cloning in progress?
   *
   * @var bool
   */
  private bool $cloning = FALSE;

  /**
   * Is reverting in progress?
   *
   * @var bool
   */
  private bool $reverting = FALSE;

  /**
   * Constructs a new GroupCloner.
   */
  public function __construct(
    GroupContentClonerManager $cloner_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxyInterface $current_user,
    StateInterface $state
  ) {
    $this->clonerManager = $cloner_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->state = $state;
  }

  /**
   * Allow other modules to check if cloning is in progress.
   */
  public function isCloning(): bool {
    @trigger_error(__METHOD__ . "() is deprecated in group_clone:1.1.0 and is removed from group_clone:1.2.0. Use the 'Drupal\Core\Entity\SynchronizableInterface::isSyncing()' method instead. See https://www.drupal.org/node/3392146", E_USER_DEPRECATED);
    return $this->cloning;
  }

  /**
   * Allow other modules to check if reverting is in progress.
   */
  public function isReverting(): bool {
    @trigger_error(__METHOD__ . "() is deprecated in group_clone:1.1.0 and is removed from group_clone:1.2.0. Use the 'Drupal\Core\Entity\SynchronizableInterface::isSyncing()' method instead. See https://www.drupal.org/node/3392146", E_USER_DEPRECATED);
    return $this->reverting;
  }

  /**
   * Sets cloning settings by content type.
   *
   * @param mixed[] $settings
   *   Settings by plugin.
   */
  public function setCloningSettings(array $settings): void {
    if (!\array_key_exists('field_config', $settings)) {
      $settings['field_config'] = [];
    }
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setClonedEntitityIds(array $cloned_entity_ids): void {
    $this->clonedEntityIDs = $cloned_entity_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getClonedEntitityIds(): array {
    return $this->clonedEntityIDs;
  }

  /**
   * Starts the group cloning operation.
   *
   * @return \Drupal\group\Entity\GroupInterface
   *   Cloned group or NULL if a batch operation has been initialized or
   *   errors occurred.
   */
  public function clone(GroupInterface $group, bool $use_batch_api = TRUE): ?GroupInterface {

    $content_items = [];
    foreach ($this->settings['plugin_config'] as $plugin_id => $plugin_config) {
      $plugin = $this->getClonerPlugin($plugin_id, $plugin_config);
      $plugin_content_ids = $plugin->getContentIdsToClone($group);
      foreach ($plugin_content_ids as $type => $ids) {
        foreach ($ids as $id) {
          $content_items[] = [$plugin_id, $type, $id];
        }
      }
    }

    $field_values = [];

    // Clone the group respecting referenced entities settings first.
    foreach ($this->settings['field_config'] as $field_id => $value) {
      if (!$value) {
        $field_values[$field_id] = self::PRESERVE_VALUE;
      }
    }

    // Override label and status of the group.
    $field_values['label'] = $group->label() . ' - Cloned';
    $field_values['status'] = FALSE;
    // On group save, owner membership may be added by default. Currently
    // it's not possible to set isSyncing = TRUE on that entity. Set owner
    // to the current user to minimize possible impact.
    $field_values['uid'] = $this->currentUser->id();

    $this->cloning = TRUE;
    $group_clone = $this->cloneEntity($group, $this->clonedEntityIDs, 'all', $field_values);
    $this->cloning = FALSE;

    if (!count($content_items)) {
      return $group_clone;
    }

    if ($use_batch_api) {
      $data = [
        'original_group_id' => $group->id(),
        'group_clone_id' => $group_clone->id(),
        'group_label' => $group->label(),
        'content_items' => $content_items,
        'settings' => $this->settings,
        'cloned_entity_ids' => $this->clonedEntityIDs,
      ];
      $batch = GroupClonerBatch::getCloneBatch($data);
      \batch_set($batch);
      return NULL;
    }
    else {
      try {
        foreach ($content_items as $item) {
          $this->cloneItem($item, $group_clone->id());
        }
        $this->storeClonedData($group->id(), $group_clone->id());
        return $group_clone;
      }
      catch (\Exception $e) {
        $this->storeClonedData($group->id(), $group_clone->id());
        throw $e;
      }
    }

    return NULL;
  }

  /**
   * Revert single item.
   */
  public function revertSingle(string $entity_type_id, string $entity_id): void {
    $this->reverting = TRUE;
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    if ($entity instanceof EntityInterface) {
      if ($entity instanceof SynchronizableInterface) {
        $entity->setSyncing(TRUE);
      }
      $entity->delete();
    }
    $this->reverting = FALSE;
  }

  /**
   * Revert cloning.
   */
  public function revert(string $cloned_group_id, bool $use_batch_api): ?Url {
    $group_storage = $this->entityTypeManager->getStorage('group');
    $cloned_group = $group_storage->load($cloned_group_id);
    $data = $this->state->get(self::STATE_PREFIX . $cloned_group_id);
    if (!$cloned_group instanceof GroupInterface || $data === NULL) {
      throw new \InvalidArgumentException('Invalid cloned group ID provided in GroupCloner::revert()');
    }
    $redirect_url = $group_storage->load($data['gid'])->toUrl();
    if ($use_batch_api) {
      $data['group_label'] = $cloned_group->label();
      $data['redirect_url'] = $redirect_url->toString();
      $data['group_clone_id'] = $cloned_group->id();
      $batch = GroupClonerBatch::getRevertBatch($data);
      \batch_set($batch);
    }
    else {
      foreach ($data['cloned_entity_ids'] as $entity_type_id => $type_entity_ids) {
        foreach ($type_entity_ids as $entity_id) {
          $this->revertSingle($entity_type_id, $entity_id);
        }
      }
      return $redirect_url;
    }
    return NULL;
  }

  /**
   * Gets cloner plugin from entity.
   */
  private function getClonerPlugin(string $plugin_id, array $plugin_config): GroupContentClonerInterface {
    if (!\array_key_exists($plugin_id, $this->clonerPlugins)) {
      $this->clonerPlugins[$plugin_id] = $this->clonerManager->createInstance($plugin_id, $plugin_config);
    }
    return $this->clonerPlugins[$plugin_id];
  }

  /**
   * Clone group content.
   */
  public function cloneItem(array $item, string $group_clone_id): void {
    [$plugin_id, $type, $entity_id] = $item;
    $plugin = $this->getClonerPlugin($plugin_id, $this->settings['plugin_config'][$plugin_id]);
    $this->cloning = TRUE;
    $plugin->clone($type, $entity_id, $group_clone_id, $this->clonedEntityIDs);
    $this->cloning = FALSE;
  }

  /**
   * Store cloned data in state to allow reverting when necessary.
   */
  public function storeClonedData(string $original_group_id, string $cloned_group_id, ?array $cloned_entity_ids = NULL) {
    if ($cloned_entity_ids === NULL) {
      $cloned_entity_ids = $this->clonedEntityIDs;
    }
    $this->state->set(self::STATE_PREFIX . $cloned_group_id, [
      'gid' => $original_group_id,
      'cloned_entity_ids' => $cloned_entity_ids,
    ]);
  }

  /**
   * Helper function to get cloning results.
   *
   * @param mixed[] $results
   *   Array of results.
   * @param throwable $exception
   *   The execption thrown during cloning or NULL.
   */
  public function getResultsMessage(array $results, \Throwable $exception = NULL): TranslatableMarkup {
    if ($exception instanceof \Throwable) {
      return $this->t('Finished with an error (@message), please inspect the result group and revert if desired.', [
        '@message' => $exception->getMessage(),
      ]);
    }

    if (!array_key_exists('group_label', $results)) {
      throw new \InvalidArgumentException('Missing group_label array key in getResultsMessage argument.');
    }
    if (!array_key_exists('group_clone_id', $results)) {
      throw new \InvalidArgumentException('Missing group_clone_id array key in getResultsMessage argument.');
    }
    if (!array_key_exists('cloned_entity_ids', $results)) {
      $results['cloned_entity_ids'] = $this->clonedEntityIDs;
    }

    $details = [];
    foreach ($results['cloned_entity_ids'] as $entity_type_id => $entity_ids) {
      $details[] = $entity_type_id . ': ' . \count($entity_ids);
    }

    $cloned_group = $this->entityTypeManager->getStorage('group')->load($results['group_clone_id']);
    return $this->t('Cloned group "@label" to "@cloned_label". Entities cloned: @details', [
      '@label' => $results['group_label'],
      '@cloned_label' => $cloned_group->label(),
      '@details' => \count($details) ? \implode(', ', $details) : '0',
    ]);
  }

}
