<?php

declare(strict_types=1);

namespace Drupal\group_clone\Routing;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * {@inheritdoc}
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  private ConfigEntityStorageInterface $configStorage;

  /**
   * Constructs a new GroupCloneAccessCheck object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->configStorage = $entity_type_manager->getStorage('group_clone_settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('entity.group.duplicate_form');
    if ($route === NULL) {
      return;
    }

    $group_types = [];
    $config_entities = $this->configStorage->loadMultiple();
    foreach ($config_entities as $entity) {
      if ($entity->status()) {
        $group_types[] = $entity->id();
      }
    }
    if (count($group_types) === 0) {
      $collection->remove('entity.group.duplicate_form');
      return;
    }
    $route_parameters = $route->getOption('parameters');
    $route_parameters['group']['bundle'] = $group_types;
    $route->setOption('parameters', $route_parameters);
  }

}
