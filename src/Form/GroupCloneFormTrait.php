<?php

declare(strict_types=1);

namespace Drupal\group_clone\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_clone\Entity\GroupCloneSettings;

/**
 * {@inheritdoc}
 */
trait GroupCloneFormTrait {

  /**
   * {@inheritdoc}
   */
  private array $settings = [];

  /**
   * Helper function to get settings entity for group type.
   */
  private function getConfigEntity(string $id): GroupCloneSettings {
    if (!array_key_exists($id, $this->settings)) {
      $group_clone_settings_storage = $this->entityTypeManager->getStorage('group_clone_settings');
      $this->settings[$id] = $group_clone_settings_storage->load($id);
      if (!$this->settings[$id] instanceof GroupCloneSettings) {
        $this->settings[$id] = $group_clone_settings_storage->create([
          'id' => $id,
          'status' => FALSE,
          'plugin_config' => [],
        ]);
      }
    }
    return $this->settings[$id];
  }

  /**
   * Get cloning settings form element for a single group type.
   */
  protected function getPluginsConfigForm(
    array &$element,
    array $plugins,
    GroupCloneSettings $config_entity,
    GroupTypeInterface $group_type,
    FormStateInterface $form_state,
    ?GroupInterface $group = NULL
  ): void {
    $plugins_config = $config_entity->get('plugin_config');
    if ($plugins_config === NULL) {
      $plugins_config = [];
    }
    foreach ($plugins as $plugin_id => $plugin) {
      if ($plugin->appliesTo($group_type)) {
        $element[$plugin_id] = [];
        $config = \array_key_exists($plugin_id, $plugins_config) ? $plugins_config[$plugin_id] : [];
        $plugin->setConfiguration($config);
        $plugin->getConfigurationForm($element[$plugin_id], $form_state, $group_type, $group);
        if (
          \count($plugins) > 0 &&
          \count($element[$plugin_id]) !== 0
        ) {
          $element[$plugin_id]['#type'] = 'fieldset';
          $element[$plugin_id]['#title'] = $plugin->getPluginDefinition()['label'];
        }
      }
    }
  }

}
