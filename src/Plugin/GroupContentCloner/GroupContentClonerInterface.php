<?php

declare(strict_types=1);

namespace Drupal\group_clone\Plugin\GroupContentCloner;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupType;

/**
 * {@inheritdoc}
 */
interface GroupContentClonerInterface {

  /**
   * Clone group content.
   *
   * @param string $type
   *   Group Content type.
   * @param string $id
   *   Group Content id.
   * @param string $group_clone_id
   *   Group Clone ID.
   * @param int[] $cloned_entity_ids
   *   Cloned Entity ID.
   */
  public function clone(string $type, string $id, string $group_clone_id, array &$cloned_entity_ids): void;

  /**
   * Get content items to clone.
   *
   * @return array
   *   Array of entity IDs keyed by a content type ID (can be any string
   *   actually that is processed by the self::clone() method $type parameter).
   */
  public function getContentIdsToClone(GroupInterface $group): array;

  /**
   * Calculate dependencies for the cloner config.
   *
   * @return array
   *   [type, name], see \Drupal\Core\Entity\DependencyTrait::addDependency.
   */
  public function calculateDependencies(GroupType $group_type): array;

  /**
   * Gets plugin configuration form.
   *
   * Add to $element that contains this plugin's configuration form.
   */
  public function getConfigurationForm(
    &$element,
    FormStateInterface $form_state,
    GroupType $group_type,
    ?GroupInterface $group = NULL
  ): void;

  /**
   * Does the cloner plugin apply to the group type?
   */
  public function appliesTo(GroupType $group_type): bool;

  /**
   * Plugin configuration getter.
   *
   * @return mixed
   *   Configuration of this plugin.
   */
  public function getConfiguration(): ?array;

  /**
   * Plugin configuration setter.
   */
  public function setConfiguration(array $configuration): void;

}
