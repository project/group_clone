### Introduction

This module allows cloning groups and their content along with any referenced
entities.


### Configuration

Configuration can be found in Groups -> Settings -> Cloning settings.
An administrator (administer group permission) can define which group types
can be cloned and what should the default cloning behavior for different
group content types be. After group cloning is enabled for a group type,
all groups of that type will have a "Clone" tab.


### Reverting

After a group is cloned, the cloning can be reverted from the "Clone" tab.
This deletes all the entities that have been cloned and doesn't affect the
source group in any way.


### Extending

To implement a specific cloning method for any group content type plugin,
create a new GroupContentCloner plugin in any module. See
`Drupal\group_clone\Annotation\GroupContentCloner` and
`Drupal\group_clone\Plugin\GroupContentCloner\GroupContentClonerInterface`.
