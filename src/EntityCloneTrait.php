<?php

declare(strict_types=1);

namespace Drupal\group_clone;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * {@inheritdoc}
 */
trait EntityCloneTrait {

  /**
   * Determines if a field is clonable.
   *
   * Based on the corresponding method from entity_clone module.
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition): bool {
    $clonable_field_types = [
      'entity_reference',
      'entity_reference_revisions',
    ];

    if (!\in_array($field_definition->getType(), $clonable_field_types, TRUE)) {
      return FALSE;
    }

    // Base fields - group_content only.
    // @todo Anything else? Configurable again?
    if ($field_definition instanceof BaseFieldDefinition) {
      if ($field_definition->getTargetEntityTypeId() !== 'group_content') {
        return FALSE;
      }
      if ($field_definition->getName() === 'entity_id') {
        return TRUE;
      }
    }

    // Standard fields.
    elseif (($field_definition instanceof FieldConfigInterface)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Clone entity.
   *
   * @return string
   *   The cloned entity ID.
   */
  protected function cloneEntity(EntityInterface $original, &$cloned_entity_ids, string $clone_method, array $new_values = []): EntityInterface {
    // If already cloned with or without references - return ID for referencing.
    if (\array_key_exists($original->getEntityTypeId(), $cloned_entity_ids) && \array_key_exists($original->id(), $cloned_entity_ids[$original->getEntityTypeId()])) {
      return $this->entityTypeManager->getStorage($original->getEntityTypeId())->load($cloned_entity_ids[$original->getEntityTypeId()][$original->id()]);
    }

    $clone = $original->createDuplicate();
    if ($clone instanceof SynchronizableInterface) {
      $clone->setSyncing(TRUE);
    }

    if ($clone_method === 'all') {
      // Borrowed from the entity_clone module and simplified a bit.
      if ($clone instanceof FieldableEntityInterface) {
        foreach ($clone->getFieldDefinitions() as $field_id => $field_definition) {
          if (\array_key_exists($field_id, $new_values)) {
            continue;
          }

          // Reset created and changed fields if exist and not explicitly set.
          if (\in_array($field_definition->getType(), ['created', 'changed'])) {
            $clone->get($field_id)->applyDefaultValue();
          }
          elseif ($this->fieldIsClonable($field_definition)) {
            $field = $clone->get($field_id);
            if ($field->count() > 0) {
              $referenced_entities = [];
              foreach ($field as $value) {
                // Check if we're not dealing with an entity
                // that has been deleted in the meantime.
                $target = $value->get('entity')->getTarget();
                if (!$target instanceof EntityAdapter) {
                  continue;
                }
                $referenced_entity = $target->getValue();

                // Don't clone nested referenced user entities.
                // Note: user entities referenced directly by group content
                // can be cloned if settings allow that.
                // @todo Make that a part of some general config?
                if ($referenced_entity->getEntityTypeId() === 'user') {
                  $referenced_entities[] = $referenced_entity;
                  continue;
                }

                $referenced_entities[] = $this->cloneEntity($referenced_entity, $cloned_entity_ids, $clone_method);
              }
              $clone->set($field_id, $referenced_entities);
            }
          }
        }
      }
    }

    // Apply new values.
    foreach ($new_values as $field => $values) {
      // Special wildcard that leaves the value as-is.
      if ($values === GroupCloner::PRESERVE_VALUE) {
        continue;
      }
      $clone->set($field, $values);
    }

    $clone->save();
    $cloned_entity_ids[$original->getEntityTypeId()][$original->id()] = $clone->id();

    return $clone;
  }

}
