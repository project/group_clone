<?php

declare(strict_types=1);

namespace Drupal\group_clone\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_clone\GroupCloner;
use Drupal\group_clone\GroupContentClonerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
final class GroupCloneForm extends FormBase {

  use GroupCloneFormTrait;

  private GroupCloner $cloner;
  private EntityTypeManagerInterface $entityTypeManager;
  private StateInterface $state;
  private GroupContentClonerManager $clonerManager;

  /**
   * Constructor.
   */
  public function __construct(
    GroupCloner $cloner,
    EntityTypeManagerInterface $entity_type_manager,
    StateInterface $state,
    GroupContentClonerManager $cloner_manager
  ) {
    $this->cloner = $cloner;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->clonerManager = $cloner_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('group_clone.cloner'),
      $container->get('entity_type.manager'),
      $container->get('state'),
      $container->get('plugin.manager.group_content_cloner')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_clone_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = NULL): array {
    $original_group = NULL;
    $clone_data = $this->state->get(GroupCloner::STATE_PREFIX . $group->id());
    if ($clone_data !== NULL) {
      $original_group = $this->entityTypeManager->getStorage('group')->load($clone_data['gid']);
      if (!$original_group instanceof GroupInterface) {
        // Original doesn't exist anymore, we can delete state data.
        $this->state->delete(GroupCloner::STATE_PREFIX . $group->id());
      }
    }

    if ($original_group instanceof GroupInterface) {
      $info = $this->t('This group has recently been cloned from @link. Press the "Revert" button to remove the clone and all of its content or "Approve" to keep the clone and be able to clone the group again.', [
        '@link' => $original_group->toLink()->toString(),
      ]);
      $op = 'revert';
      $op_text = $this->t('Revert');
    }
    else {
      $info = $this->t('You are about to clone the "@label" group. Please confirm.', [
        '@label' => $group->label(),
      ]);
      $op = 'clone';
      $op_text = $this->t('Clone');
    }

    $form['info'] = [
      '#markup' => $info,
    ];

    if ($op === 'clone') {
      $group_type = $group->getGroupType();
      $plugins = $this->clonerManager->getPlugins();
      $config_entity = $this->getConfigEntity($group_type->id());
      $form['plugin_config'] = [];
      $this->getPluginsConfigForm($form['plugin_config'], $plugins, $config_entity, $group_type, $form_state, $group);
      foreach (\array_keys($form['plugin_config']) as $key) {
        if (\str_starts_with('#', $key)) {
          continue;
        }
        if (\count($form['plugin_config'][$key]) !== 0) {
          $form['plugin_config']['#type'] = 'details';
          $form['plugin_config']['#title'] = $this->t("Plugins' settings");
          $form['plugin_config']['#open'] = FALSE;
          $form['plugin_config']['#tree'] = TRUE;
        }
      }
    }

    $form['batch'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Process in a batch operation (recommended)'),
      '#default_value' => TRUE,
    ];
    $form['actions'] = ['#type' => 'actions'];

    $form['actions'][$op] = [
      '#type' => 'submit',
      '#value' => $op_text,
    ];
    if ($op === 'revert') {
      $form['actions']['approve'] = [
        '#type' => 'submit',
        '#value' => $this->t('Approve'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $op = $form_state->getTriggeringElement()['#parents'][0];
    $group = $form_state->getBuildInfo()['args'][0];
    $use_batch_api = (bool) $form_state->getValue('batch');

    if ($op === 'clone') {
      $config_entity = $this->getConfigEntity($group->bundle());
      $plugin_config = $config_entity->get('plugin_config');
      foreach ($form_state->getValue('plugin_config', []) as $plugin_id => $config) {
        foreach ($config as $key => $value) {
          $plugin_config[$plugin_id][$key] = $value;
        }
      }
      $field_config = $config_entity->get('field_config');

      $this->cloner->setCloningSettings([
        'plugin_config' => $plugin_config,
        'field_config' => $field_config,
      ]);
      if ($use_batch_api) {
        $this->cloner->clone($group, $use_batch_api);
      }
      else {
        try {
          $cloned_group = $this->cloner->clone($group, $use_batch_api);
          $results = [
            'group_label' => $group->label(),
            'group_clone_id' => $cloned_group->id(),
          ];
          $this->messenger()->addStatus($this->cloner->getResultsMessage($results));
          $form_state->setRedirectUrl($cloned_group->toUrl('edit-form'));
        }
        catch (\Exception $e) {
          $this->messenger()->addError($this->cloner->getResultsMessage([], $e));
        }
      }
    }
    elseif ($op === 'revert') {
      if ($use_batch_api) {
        $this->cloner->revert($group->id(), TRUE);
      }
      else {
        $redirect_url = $this->cloner->revert($group->id(), FALSE);
        $this->state->delete(GroupCloner::STATE_PREFIX . $group->id());
        $this->messenger()->addStatus('Cloned group @group and its content has been removed.', [
          '@group' => $group->label(),
        ]);
        $form_state->setRedirectUrl($redirect_url);
      }
    }
    elseif ($op === 'approve') {
      $this->state->delete(GroupCloner::STATE_PREFIX . $group->id());
    }
  }

}
