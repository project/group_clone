<?php

declare(strict_types=1);

namespace Drupal\group_clone\Plugin\GroupContentCloner;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupType;

/**
 * Default group content cloner plugin.
 *
 * @GroupContentCloner(
 *   id = "default",
 *   label = "Default cloner",
 *   content_plugin_id = "_all",
 * )
 */
class DefaultGroupContentCloner extends GroupContentClonerBase {

  /**
   * {@inheritdoc}
   */
  public function getConfigurationForm(
    &$element,
    FormStateInterface $form_state,
    GroupType $group_type,
    ?GroupInterface $group = NULL
  ): void {
    parent::getConfigurationForm($element, $form_state, $group_type);

    // Don't show settings on group clone form.
    if ($group !== NULL) {
      return;
    }

    $element['content_type_behavior'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content type settings'),
    ];

    $content_types_config = \array_key_exists('content_type_behavior', $this->configuration) ? $this->configuration['content_type_behavior'] : [];

    $group_content_type_storage = $this->entityTypeManager->getStorage('group_content_type');
    foreach ($group_content_type_storage->loadByGroupType($group_type) as $group_content_type) {
      $type_id = $group_content_type->id();
      $element['content_type_behavior'][$type_id] = [
        '#type' => 'radios',
        '#title' => $group_content_type->label(),
        '#options' => [
          'disabled' => $this->t('Cloning disabled'),
          'references' => $this->t('References only'),
          'all' => $this->t('References and entities'),
        ],
        '#default_value' => \array_key_exists($type_id, $content_types_config) ? $content_types_config[$type_id] : 'disabled',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(GroupType $group_type): array {
    $dependencies = [];
    foreach ($this->getConfiguration('content_type_behavior', []) as $group_content_type_id => $value) {
      if ($value !== 0) {
        $group_content_type = $this->entityTypeManager
          ->getStorage('group_content_type')
          ->load($group_content_type_id);
        if ($group_content_type) {
          $dependencies[] = [
            'config',
            $group_content_type->getConfigDependencyName(),
          ];
        }
      }
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    // Clear disabled content types.
    if (\array_key_exists('content_type_behavior', $configuration)) {
      foreach ($configuration['content_type_behavior'] as $type_id => $value) {
        if ($value === 'disabled') {
          unset($configuration['content_type_behavior'][$type_id]);
        }
        else {
          $configuration['content_type_behavior'][$type_id] = $value;
        }
      }
    }

    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentIdsToClone(GroupInterface $group): array {
    $output = [];
    $group_content_storage = $this->entityTypeManager->getStorage('group_content');
    foreach ($this->configuration['content_type_behavior'] as $type_id => $status) {
      if ($status === 'disabled') {
        continue;
      }
      $query = $group_content_storage->getQuery();
      $output[$type_id] = $query
        ->accessCheck(FALSE)
        ->condition('gid', $group->id())
        ->condition('type', $type_id)
        ->execute();
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function clone(string $type, string $id, string $group_clone_id, array &$cloned_entity_ids): void {
    $group_content = $this->entityTypeManager->getStorage('group_content')->load($id);
    if (!$group_content instanceof GroupContentInterface) {
      return;
    }
    $clone_method = $this->configuration['content_type_behavior'][$type];

    $this->cloneEntity($group_content, $cloned_entity_ids, $clone_method, [
      'gid' => $group_clone_id,
    ]);
  }

}
