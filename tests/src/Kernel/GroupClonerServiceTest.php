<?php

declare(strict_types=1);

namespace Drupal\Tests\group_clone\Kernel;

use Drupal\Tests\group\Kernel\GroupKernelTestBase;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_clone\GroupCloner;

/**
 * Tests for the GroupCloner service.
 *
 * @group group_clone
 *
 * @coversDefaultClass \Drupal\group_clone\GroupCloner
 */
final class GroupClonerServiceTest extends GroupKernelTestBase {

  /**
   * {@inheritdoc}
   */
  private GroupCloner $groupCloner;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'group_clone',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->groupCloner = $this->container->get('group_clone.cloner');
  }

  /**
   * {@inheritdoc}
   */
  public function testGroupCloner(): void {
    $original = $this->createGroup([
      'label' => 'test group',
    ]);
    $account = $this->createUser();
    $original->addContent($account, 'group_membership');

    $this->groupCloner->setCloningSettings([
      'default' => [
        'content_type_behavior' => [
          $original->bundle() . '-group_membership' => 'references',
        ],
      ],
    ]);
    $clone = $this->groupCloner->clone($original, FALSE);
    $this->assertTrue($clone instanceof GroupInterface);

  }

}
